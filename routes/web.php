<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();
Route::get('/home', 'HomeController@index');
Route::group(['middleware' => 'auth'], function() {
    Route::resource('partner', 'PartnerController');
    Route::resource('user', 'UserController'); 
    Route::get('/partnerexport', ['as' => 'partnerexport', 'uses' => 'PartnerController@export']);
});