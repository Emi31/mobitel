<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Redirect;
use App\Partner;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;


class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partner = new Partner();
        return view('partner/form', compact('partner'))->with(['action' => 'add' ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $input = Request::all();
        $validator = Validator::make(Request::all(), [
            'full_name' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $partner = new Partner();
        $partner->full_name = $input['full_name'];
        $partner->user_id = Auth::user()->id;
        $partner->save();
        return redirect()->route('partner.edit',$partner->id)->with('info', 'User added, Name is: ' . $input['full_name']);

        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::find($id);
        return view('partner/form_edit', compact('partner'))->with(['action' => 'edit' ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = Request::all();
        $validator = Validator::make(Request::all(), [
            'full_name' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $partner = Partner::find($id);
        $partner->full_name = $input['full_name'];
        $partner->user_id = Auth::user()->id;
        $partner->save();
        return redirect()->route('partner.edit',$partner->id)->with('info', 'User edited, Name is: ' . $input['full_name']);

    }

    public function export(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        
        $partners =Partner::with('user')->get();
        $columns = array('Full Name', 'Name');
        
        

        $callback = function() use ($partners, $columns)
        {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($partners as $partner) {
            fputcsv($file, array($partner->full_name, $partner->user->name));
        }
        fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }   
}
