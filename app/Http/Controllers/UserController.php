<?php

namespace App\Http\Controllers;

use Request;
use Redirect;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return User:: with('user')->get();
        //if(Auth::user()->id==3){
        $users = User::where('id', '!=', Auth::id())->get();

        //}
        
        return view('user/index', compact('users'));
        // $users = User:: with('user')->get();
        // $data['users'] = array();
        // foreach ($users as $var) {
        //     if($var->code !=''){
        //         $data['users'][$var->code] = array(
        //                 'name' => $var->full_name,
        //                 'phone' => $var->mobile,
        //                 'address' => '',
        //                 'size1' => $var->size1,
        //                 'size2' => $var->size2,
        //                 'size3' => $var->size3,
        //                 'size4' => $var->size4,
        //                 'size5' => $var->size5,
        //                 'size6' => $var->size6,
        //                 'size7' => $var->size7,
        //                 'size8' => $var->size8,
        //                 'size9' => $var->size9,
        //                 'pocket' => $var->pocket,
        //                 'keraType' => $var->kera_type,
        //                 'note' => $var->note,
        //                 'userId' => "03217548720"
        //             );
        //     }
            
        // }

        // echo json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        return view('user/form', compact('user'))->with(['action' => 'add' ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = Request::all();
        $validator = Validator::make(Request::all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        
        $user = new User();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->save();
        return redirect()->route('user.index')->with('info', 'User added, Name is: ' . $input['name']);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User:: with('user')->whereUserId($id)->get();
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user/form_edit', compact('user'))->with(['action' => 'edit' ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Request::all();
        $validator = Validator::make(Request::all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $user = User::find($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->save();
        
        return redirect()->route('user.index')->with('info', 'Customer edited, Name is: ' . $input['name']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (!$user) {
            session()->flash('msg', 'Required Data missing');
            return Redirect::back();
        }
        $user->delete();
        
        
        session()->flash('msg', 'Deleted');
        return Redirect::back();
    }
}
