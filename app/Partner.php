<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;
    public $table = "partner";

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'code', 'mobile', 'size1', 'size2', 'size3', 'size4', 'size5',
        'size6', 'size7', 'size8', 'size9', 'pocket', 'kera_type', 'note'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
