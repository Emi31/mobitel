-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.29 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table tailor.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `size1` decimal(10,2) NOT NULL,
  `size2` decimal(10,2) NOT NULL,
  `size3` decimal(10,2) NOT NULL,
  `size4` decimal(10,2) NOT NULL,
  `size5` decimal(10,2) NOT NULL,
  `size6` decimal(10,2) NOT NULL,
  `size7` decimal(10,2) NOT NULL,
  `size8` decimal(10,2) NOT NULL,
  `size9` decimal(10,2) NOT NULL,
  `pocket` varchar(10) NOT NULL,
  `kera_type` varchar(10) NOT NULL,
  `note` varchar(512) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

-- Dumping data for table tailor.customers: ~4 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `full_name`, `code`, `mobile`, `size1`, `size2`, `size3`, `size4`, `size5`, `size6`, `size7`, `size8`, `size9`, `pocket`, `kera_type`, `note`, `created_at`, `user_id`, `deleted_at`, `updated_at`) VALUES
	(1, 'Waqar Ali', '', '03321234567', 40.50, 18.25, 24.50, 38.25, 15.00, 22.00, 11.00, 41.00, 18.00, '2+1', 'Gol', 'This is first entry for customer', '2016-08-24 19:32:40', 1, NULL, '2016-08-27 12:48:38'),
	(3, 'Faizan Rashid', '', '03466222858', 42.50, 18.25, 23.50, 18.00, 15.00, 36.00, 34.00, 42.00, 10.00, '2+1', 'Gol', 'Faizan bhai suit maap', '2016-08-27 16:41:07', 1, NULL, '2016-08-27 11:41:07'),
	(5, 'Fahad mehar', '', '03316682383', 38.50, 17.50, 23.00, 30.00, 13.00, 20.50, 0.00, 38.50, 17.00, '2+1', 'State', '', '2017-01-26 12:46:22', 2, NULL, '2017-01-26 07:48:29'),
	(6, 'Qari ishtiaq attari', '', '', 45.00, 18.00, 25.00, 40.00, 16.00, 31.00, 0.00, 36.50, 18.00, '1+2+1', 'Stat', '', '2017-01-26 12:48:35', 2, NULL, '2017-01-26 07:50:50'),
	(7, 'Basharat ali', '', '', 43.50, 18.50, 24.50, 39.50, 16.00, 27.00, 0.00, 40.00, 18.00, '4', 'State', '', '2017-01-26 12:52:41', 2, NULL, '2017-01-26 07:52:41'),
	(8, 'Nasir maseeh aizaq', '', '', 36.00, 17.50, 22.50, 36.00, 13.50, 0.00, 0.00, 37.00, 16.00, '2+1', 'State', '', '2017-01-26 12:54:36', 2, NULL, '2017-01-26 07:54:36'),
	(9, 'Wajpaii', '', '', 36.00, 17.00, 22.00, 35.00, 13.50, 21.50, 0.00, 38.50, 16.00, '2+1', 'State', '', '2017-01-26 12:56:31', 2, NULL, '2017-01-26 07:56:31'),
	(10, 'Muhamad shahbaz attari /sarfraz attari', '', '03364663418', 40.00, 19.50, 22.50, 37.00, 15.00, 23.50, 0.00, 38.50, 16.00, '2+1', 'State', '', '2017-01-26 12:59:38', 2, NULL, '2017-01-26 07:59:38'),
	(11, 'Sarfraz attari', '', '03364663418', 40.00, 18.00, 22.50, 34.00, 14.00, 23.00, 0.00, 36.00, 18.00, '4', 'State', 'Miswaq pocket/5by 6.50\nBaba bazo mory 6t', '2017-01-26 13:02:46', 2, NULL, '2017-01-26 08:02:46'),
	(12, 'Adnan maseeh', '', '', 38.00, 18.50, 22.00, 38.00, 14.50, 22.50, 0.00, 38.50, 18.00, '2+1', 'State', '', '2017-01-26 13:04:26', 2, NULL, '2017-01-26 08:04:26'),
	(13, 'Malik leyakat genrol store', '', '', 40.00, 19.00, 23.75, 42.00, 15.50, 28.50, 0.00, 36.00, 18.00, '3', 'State', '', '2017-01-26 13:07:15', 2, NULL, '2017-01-26 08:07:15'),
	(14, 'Saleem genrol store', '', '', 38.50, 18.50, 23.00, 42.00, 15.50, 23.50, 0.00, 38.50, 18.00, '2+1', 'State', '', '2017-01-26 13:09:22', 2, NULL, '2017-01-26 08:09:22'),
	(15, 'Awais printing', '637', '03026429815', 43.00, 19.00, 25.00, 38.00, 14.50, 22.00, 0.00, 43.50, 18.00, '2+1', 'State', '', '2017-01-26 13:10:04', 2, NULL, '2017-01-26 08:12:37'),
	(16, 'Khuram mhr/awais printing', '643', '03026429815', 39.50, 17.50, 24.50, 36.00, 15.00, 23.50, 0.00, 39.50, 16.00, '2+1', 'Gowl', '', '2017-01-26 13:15:04', 2, NULL, '2017-01-26 08:15:34'),
	(17, 'Kuram mehar/shoaib mhr', '', '', 40.00, 18.00, 24.00, 40.00, 16.00, 22.50, 0.00, 39.50, 18.00, '2+1', 'State', '', '2017-01-26 13:15:05', 2, NULL, '2017-01-26 08:19:02'),
	(18, 'Waheedu zaman mughal', '645', '03152608400', 40.00, 18.50, 24.00, 38.00, 15.50, 22.50, 0.00, 40.00, 18.00, '2+2', 'State', '', '2017-01-29 09:49:07', 2, NULL, '2017-01-29 04:51:23'),
	(19, 'Zafer iqbal', '646', '646', 40.50, 18.00, 25.50, 45.00, 14.00, 28.50, 0.00, 38.00, 18.00, '3', 'Gowl', '', '2017-01-29 09:51:03', 2, NULL, '2017-01-29 04:51:40'),
	(20, 'Tanvir mhr/shoaib mhr', '647', '647', 37.00, 18.00, 21.50, 37.00, 14.00, 23.00, 0.00, 38.00, 18.00, '4', 'State', '', '2017-01-29 09:53:05', 2, NULL, '2017-01-29 04:53:05'),
	(21, 'Gulzar ahmad', '648', '648', 37.00, 18.00, 20.50, 36.00, 14.50, 25.00, 0.00, 35.50, 18.00, '2+1', 'Gowl', '', '2017-01-29 09:54:29', 2, NULL, '2017-01-29 04:54:29'),
	(22, 'Farhan ali/awais printing', '649', '649', 41.50, 19.00, 26.50, 40.00, 15.00, 22.50, 0.00, 42.00, 18.00, '4', 'State', '', '2017-01-29 09:56:07', 2, NULL, '2017-01-29 04:56:07'),
	(23, 'Amin khan', '700', '700/034444444444', 36.50, 18.50, 22.00, 36.50, 15.00, 26.50, 0.00, 35.00, 18.00, '4', 'Gowl', 'Bain\\duble silae', '2017-01-29 09:56:10', 2, NULL, '2017-02-18 18:26:09'),
	(24, 'Memod jutt', '', '650', 39.00, 17.50, 22.00, 38.00, 15.00, 22.00, 0.00, 38.00, 17.00, '2+1', 'State', '', '2017-01-29 09:57:31', 2, NULL, '2017-01-29 04:57:31'),
	(25, 'Afaaq mukhtar colony', '', '651', 35.00, 16.50, 21.50, 33.00, 14.00, 19.50, 0.00, 38.00, 17.00, '2+1', 'State', '', '2017-01-29 09:59:02', 2, NULL, '2017-01-29 04:59:02'),
	(26, 'Salman attari', '', '652', 38.50, 18.00, 24.00, 40.00, 15.50, 23.00, 0.00, 37.50, 18.00, '2+1', 'State', '', '2017-01-29 10:00:24', 2, NULL, '2017-01-29 05:00:24'),
	(27, 'Numan ahmad', '', '654', 37.00, 18.00, 21.50, 43.00, 16.50, 26.00, 0.00, 36.00, 18.00, '4', 'Gowl', '', '2017-01-29 10:03:21', 2, NULL, '2017-01-29 05:03:21'),
	(28, 'Salman attari mhr', '', '655', 37.00, 16.00, 34.00, 33.00, 13.00, 19.00, 0.00, 39.50, 17.00, '2+1', 'State', '', '2017-01-29 10:04:41', 2, NULL, '2017-01-29 05:04:41'),
	(29, 'Abd ur razzaq green vally', '', '656', 39.00, 18.50, 24.00, 36.50, 14.50, 21.50, 0.00, 40.00, 17.00, '4', 'State', '', '2017-01-29 10:06:19', 2, NULL, '2017-01-29 05:06:19'),
	(30, 'Saleem jojo', '', '659/03029295424', 42.00, 19.50, 26.00, 42.00, 15.50, 26.00, 0.00, 42.00, 18.00, '2+1', 'State', '', '2017-01-29 10:07:52', 2, NULL, '2017-01-29 05:08:20'),
	(31, 'Hafiz nadeem mhr', '', '661/03007410312', 38.50, 17.00, 22.00, 36.00, 14.00, 22.00, 0.00, 36.50, 18.00, '2+1', 'State', 'Bain', '2017-01-29 10:10:38', 2, NULL, '2017-01-29 05:10:38'),
	(32, 'Abdur rasheed pakorry waly', '', '665', 44.50, 20.00, 25.00, 43.00, 16.00, 34.00, 0.00, 40.00, 17.00, '4', 'Gowl', 'Dabot wali patti\nBack tera daboty', '2017-01-29 10:13:26', 2, NULL, '2017-01-29 05:13:26'),
	(33, 'Azeem mistri k walid', '', '666', 39.00, 17.50, 24.00, 38.00, 14.00, 26.00, 0.00, 0.00, 0.00, '1samny 1 s', 'Gowl', '', '2017-01-29 10:16:04', 2, NULL, '2017-01-29 05:16:04'),
	(34, 'Shafiq ahmad/tayyab karyana', '', '667', 41.00, 18.50, 23.50, 40.00, 15.00, 26.00, 0.00, 38.00, 18.00, '3', 'State', '', '2017-01-29 10:17:22', 2, NULL, '2017-01-29 05:17:22'),
	(35, 'Muhammad irshad', '', '668', 40.50, 18.00, 24.00, 40.00, 16.00, 25.00, 0.00, 41.00, 18.00, '2+1', 'State', '', '2017-01-29 10:18:36', 2, NULL, '2017-01-29 05:18:36'),
	(36, 'Muhammad boota dhol', '', '673/03466051376', 39.50, 20.00, 23.50, 42.50, 17.00, 26.00, 0.00, 40.50, 18.00, '4', 'State', 'Bain', '2017-01-29 10:20:53', 2, NULL, '2017-01-29 05:20:53'),
	(37, 'Muhammad boota dhol', '', '673/03466051376', 39.50, 20.00, 23.50, 42.50, 17.00, 26.00, 0.00, 40.50, 18.00, '4', 'State', 'Bain', '2017-01-29 10:20:53', 2, NULL, '2017-01-29 05:20:53'),
	(38, 'Sabir malik', '', '679', 37.50, 18.00, 23.75, 38.00, 14.50, 23.00, 0.00, 40.00, 20.00, '3', 'State', 'Kaff 10  duble silae', '2017-01-29 14:41:24', 2, NULL, '2017-01-29 09:41:24'),
	(39, 'Shahzaad butt jagna', '', '683', 37.00, 18.00, 24.00, 40.00, 15.00, 22.00, 0.00, 41.00, 18.00, '2+1', 'State', '', '2017-01-29 14:42:56', 2, NULL, '2017-01-29 09:42:56'),
	(40, 'Zulfiqar ahmad/rukhsaar ahmad', '', '684', 38.00, 18.00, 24.00, 36.00, 13.50, 25.00, 0.00, 41.00, 17.00, '2+1', 'State', '', '2017-01-29 14:44:58', 2, NULL, '2017-01-29 09:44:58'),
	(41, 'Jastan masseh', '', '685', 39.00, 17.50, 22.50, 35.50, 14.50, 21.50, 0.00, 38.50, 18.00, '', 'State', '', '2017-01-29 14:46:09', 2, NULL, '2017-01-29 09:46:09'),
	(42, 'Muhammad tariq mughal', '', '675', 42.00, 18.50, 25.00, 38.00, 16.00, 26.00, 0.00, 40.00, 19.00, '4', 'State', '', '2017-01-29 16:08:50', 2, NULL, '2017-01-29 11:08:50'),
	(43, 'Sufyan shareef', '', '681', 38.50, 17.50, 24.50, 37.00, 14.00, 22.00, 0.00, 41.00, 17.00, '2+1', 'State', '', '2017-01-29 16:11:38', 2, NULL, '2017-01-29 11:11:38'),
	(44, 'Muhammad zahir babu lohar', '', '687', 40.00, 18.50, 24.00, 41.00, 15.00, 23.50, 0.00, 39.00, 18.00, '2+1', 'State', '', '2017-01-29 16:13:37', 2, NULL, '2017-01-29 11:13:37'),
	(45, 'Muhammad fayaz', '', '689', 39.00, 18.00, 24.50, 38.00, 14.50, 21.50, 0.00, 41.00, 18.00, '2+1', 'State', '', '2017-01-29 16:14:44', 2, NULL, '2017-01-29 11:14:44'),
	(46, 'Peer muhammad patwari', '', '690', 41.50, 19.50, 23.50, 46.00, 17.50, 30.00, 0.00, 40.50, 18.00, '4', 'Gowl', '', '2017-01-29 16:15:55', 2, NULL, '2017-01-29 11:32:24'),
	(47, 'Ali raza mhr mukhtar colony', '', '691/03136924245', 41.00, 19.00, 24.50, 38.00, 14.50, 21.50, 10.50, 40.00, 18.00, '2+1', 'State', '', '2017-01-29 16:20:48', 2, NULL, '2017-01-29 11:20:48'),
	(48, 'Khuram maseeh', '', '693', 40.00, 19.00, 24.00, 36.00, 14.50, 21.00, 10.25, 40.50, 16.00, '2+1', 'State', '', '2017-01-29 16:23:19', 2, NULL, '2017-01-29 11:23:19'),
	(49, 'Muhammad inayet\\ nadim cycle', '', '696', 40.00, 19.50, 24.00, 42.00, 16.00, 32.00, 0.00, 36.50, 17.00, '3', 'Gowl', '', '2017-01-29 16:25:57', 2, NULL, '2017-01-29 11:25:57'),
	(50, 'Amin khan', '', '700', 36.50, 18.50, 22.00, 36.50, 15.00, 26.50, 0.00, 35.00, 18.00, '4', 'Gowl', 'Bain\\ duble silae', '2017-01-29 16:28:12', 2, NULL, '2017-01-29 11:28:12'),
	(51, 'Haji muzammil', '', '03005665955', 42.50, 19.00, 24.00, 42.50, 16.50, 24.00, 0.00, 39.50, 18.00, '4', 'State', '', '2017-01-29 16:34:29', 2, NULL, '2017-01-29 11:34:29'),
	(52, 'Haji muzzamil', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '2017-01-29 16:36:07', 2, NULL, '2017-01-29 11:36:07'),
	(53, 'Shafaqat ali wania wala', '', '03206730664', 40.00, 18.00, 23.50, 38.00, 14.00, 25.00, 0.00, 39.00, 18.00, '4', 'State', '', '2017-01-29 16:38:06', 2, NULL, '2017-01-29 11:38:06'),
	(54, 'Hjk', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '2017-01-29 16:38:48', 2, NULL, '2017-01-29 11:38:48'),
	(55, 'Sgi8o', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '2017-01-29 16:44:58', 2, NULL, '2017-01-29 11:44:58'),
	(56, 'Zeeshan Rasheed', '345', '03466160401', 45.00, 45.00, 34.00, 32.00, 12.00, 34.00, 23.00, 54.00, 32.00, '2+1', 'Gool', 'This is test entry edited', '2017-02-18 19:09:56', 1, NULL, '2017-02-18 19:10:59');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table tailor.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table tailor.migrations: 2 rows
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(3, '2014_10_12_000000_create_users_table', 1),
	(4, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table tailor.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table tailor.password_resets: 0 rows
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table tailor.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table tailor.users: 2 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Zeeshan Rasheed', 'engr.zrg@gmail.com', '$2y$10$AQISsOEHoP5djETW1WamuuOvDKdHdNyk/t62eixtttD6TPzK4gm8y', 1, 'J7BHtFE6kgIZRDKi6BQxhLHhHab7QNPX6BRF2fyly0euNVvUJxKzP308QV1C', '2017-02-18 11:15:23', '2017-02-18 14:13:52'),
	(2, 'Waqar', 'waqar@gmail.com', '$2y$10$xbkV5jF1gmXHoEiqNTdm8OGd4mRcWGA5AKI1Lm2zY4hVibSJOrm7y', 1, '7z26QS1KISimJqXNhxGQqCVKhl05e9DirWbED4o7nr6cUACoFqiTiMjVwCmN', '2017-02-18 11:41:49', '2017-02-18 12:01:29');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
