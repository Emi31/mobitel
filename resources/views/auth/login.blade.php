@extends('layouts.appLogin')
@section('title')
    Admin - Login
@endsection
@section('contents')
    <div class="body">
        <form id="sign_in" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
           <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="email" placeholder="Username / Email" required autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
            </div>
            <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                <div class="form-line">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-8 p-t-5">
                </div>
                <div class="col-xs-4">
                    <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                </div>
            </div>
            <div class="row m-t-15 m-b--20">
                <!-- <div class="col-xs-6">
                    <a href="{{ route('register') }}">Register Now!</a>
                </div> -->
                <div class="col-xs-6 align-right">
                    <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::to('assets/js/pages/examples/sign-in.js') }}"></script>
@endsection
