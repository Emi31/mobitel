<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $auth->name }}</div>
            <div class="email">{{ $auth->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                     <li><a href="{{ url('/logout') }}"
                              onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Sign Out</a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            @if(Auth::user()->is_active==0)         
            <li>
                <a href="{{ route('partner.index') }}">
                    <i class="material-icons">group</i>
                    <span>Profile Partner</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->is_active==1)         
            <li>
            <a href="{{ route('user.index') }}">
                    <i class="material-icons">group</i>
                    <span>User</span>
                </a>
            </li>
            <li>
            <a href="{{ route('partnerexport') }}">
                    <i class="material-icons">exit_to_app</i>
                    <span>Export</span>
                </a>
            </li>
            @endif
            
            
            {{--<li>--}}
                {{--<a href="../../pages/typography.html">--}}
                    {{--<i class="material-icons">text_fields</i>--}}
                    {{--<span>Typography</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="../../pages/helper-classes.html">--}}
                    {{--<i class="material-icons">layers</i>--}}
                    {{--<span>Helper Classes</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->
<!-- Right Sidebar -->
