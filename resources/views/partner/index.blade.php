@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @include('partials.alerts')
        <!-- Customers List -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        Partners Profile
                        </h2>


                    </div>


                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ route('customer.create') }}" class="btn btn-success pull-right">New Partners Profile</a>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Mobile #</th>
                            </tr>
                            </thead>
                            {{--<tfoot>--}}
                            {{--<tr>--}}
                                {{--<th>Name</th>--}}
                                {{--<th>Code</th>--}}
                                {{--<th>Phone</th>--}}
                                {{--<th>Action</th>--}}
                            {{--</tr>--}}
                            {{--</tfoot>--}}
                            <tbody>
                            @forelse ($customers as $customer)
                                <tr>
                                    <td><a href="{{ route('customer.edit', ['id' => $customer->id ]) }}"> {{ $customer->full_name }}</a></td>
                                    <td>{{ $customer->code }}</td>
                                    <td>{{ $customer->mobile }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No Record Found!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
@endsection