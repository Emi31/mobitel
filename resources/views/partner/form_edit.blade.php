@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @include('partials.errors')
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            @if($action=='edit')
                                Update Partner
                            @else
                                Create New Partner
                            @endif
                        </h2>

                    </div>
                    <div class="body">
                        <form id="form_validation" method="POST" action="{{ ($action=='edit')?route('partner.update', ['id' => $partner->id]):route('partner.store') }}">
                            @if($action=="edit")
                                {{ method_field('PUT') }}
                            @endif
                            {{ csrf_field() }}
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="full_name" id="form-name" value="{{ $partner->full_name }}" required>
                                </div>
                            </div>

                            <button class="btn btn-success waves-effect" type="submit">Update</button>
                            <a href="{{ route('partner.index') }}" class="btn btn-primary waves-effect">New</a>
                           
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#form_validation').validate({
                rules: {
                    'checkbox': {
                        required: true
                    },
                    'gender': {
                        required: true
                    }
                },
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
@endsection