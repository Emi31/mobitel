@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @include('partials.alerts')
        <!-- Customers List -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        User
                        </h2>


                    </div>


                    <div class="body">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ route('user.create') }}" class="btn btn-success pull-right">New User</a>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                           
                            <tbody>
                            @forelse ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                    <form method="POST" action="{{route('user.destroy',$user->id)}}" accept-charset="UTF-8">
                                    {{ csrf_field() }}    
                                    <input name="_method" type="hidden" value="DELETE">
                                    <a href="{{route('user.edit',$user->id)}}" class="btn btn-warning waves-effect">Edit</a>
                                   
                                    <button type="submit" class="btn btn-warning waves-effect">Delete</button>
                                    </form>    
                                     </td>                                    
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No Record Found!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
@endsection